print("sa")

import Request.Get as Get
import Request.Put as Put
import Request.Delete as Delete


mapper = {
    "GET"   : Get.Get,
    "PUT"   : Put.Put,
    "DELETE": Delete.Delete
    }

def Manager(_type):
    return mapper[_type]
