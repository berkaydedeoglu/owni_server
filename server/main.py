import socket 
import Request, Response, Listener

def main(host, port):
    api = Listener.ApiListener()
    api.create_socket(host, port)

    api.run_api()


if __name__ == "__main__":
    import sys
    
    host, port = sys.argv[1], int(sys.argv[2])

    main(host, port)

    
