import Request.Request as Request
import Payload

class Get(Request.Request):
    def __init__(self, message):
        super().__init__(message)        

    @property
    def request_type(self):
        return "GET"

    def generate_response(self):
        check = super().generate_response()
        
        if check:
            return check

        payload = Payload.Payload(self.header["path"])

        import Response.R_Ok as Ok
        return Ok.Ok(payload)
    
    
if __name__ == "__main__":
    r = Get(b"""GET /files/dir.bilge HTTP/1.1
Host: net.tutsplus.com
User-Agent: Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5 (.NET CLR 3.5.30729)
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-us,en;q=0.5
Accept-Encoding: gzip,deflate
Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7
Keep-Alive: 300
Connection: keep-alive
Cookie: PHPSESSID=r2t5uvjq435r4q7ib3vtdjq120
Pragma: no-cache
Cache-Control: no-cache""")

    print(r.response)
