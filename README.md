# Owni Server

HTTP protokolünü ilkel biçimde kullanarak yine ilkel sorgulara özgün cevaplar verebilen bir HTTP sunucusu ve bu sunucuyu test edebilecek bir HTTP istemci geliştirme.

## Proje Amaçları


* GET, PUT, DELETE isteklerini okumak, yorumlamak, ve isteklerini sunucu bilgisayar üzerinde çalıştırmak.

* Gelen isteklere uygun farklı cevaplar oluşturmak ve onları karşı tarafa iletmek.

* Tüm iletişimi TCP üzerinden yükselttiğimiz ilkel http sunucu üzerinden sağlamak ve bu sunucunun modern web tarayıcılarına dahi hizmet verebilmesini sağlamak.



## Yöntem

### Kullanılan Dil ve Test Ortamı


Projenin ana yazılım dili Python3’tür. Birçok alan takım geliştiricisine bağlı olarak nesne yönelimli olarak yazılmış olsa da hatrı sayılır biçimde yapısal felsefede geliştirmeler de yapılmıştır.

Projenin Python alanları genel olarak GCC 7.3.0 [linux] üzerinde koşan Python 3.6.5 yorumlayıcısı kullanılarak hazırlanmıştır.

Mini testler Windows 10, Ubuntu SMP [Linux Kernel 4.15.0-29 Release], Manjaro [Linux Kernel 4.19.0] ve Fedora29 [Linux Kernel 4.19.04-300 Release] işletim sistemleri üzerinde yapılmıştır.

Projenin sunucu ya da istemci ayağı standart Python3 kütüphaneleri dışında başka **bağımlılık bulundurmamaktadır.**

En iyi sonuçlar istemci olarak Curl kullanıldığında alınmasına karşın yazılan test istemcisi, Chrome, Firefox, MS Edge gibi modern tarayıcılarda test edilmiştir. (Bkz. Test)
Geliştirici editörleri yine takım geliştiricisine bağlı olarak 

* GNU/Emacs,
* Notepad++,
* Vim,
* PyCharm 

olup tamamı indent_mode=4(spaces) olarak ayarlıdır.

### Karşılıklı Haberleşme

Uçlar arası haberleşme klasik HTTP protokolünde beklendiği gibi _(bkz. Rfc2616)_ Ipv4 ve **TCP** protokolü üzerine kuruludur. Bu protokoller üzerine HTTP headerları olarak çıkılır. Genel sistem Linux üzerine planladığından _\r\n_ karakterleri UTF-8 kodlamasına göredir ve bu Windows işletim sisteminde hatalara yol açabilmektedir.
Genel anlamda headerlar ve altlarında aktarılan veriler biçiminde **byte tüpleri** yoluyla veri aktarılmıştır.

### Büyük Veri Alma Çözümü

Taraflar birbirlerine beklenenden büyük veri gönderdiğinde _recv()_ metodu kendisine verilen parametre boyutunda veri çekiyor ve bekliyordu. Bunun çözümü olarak ya beklenen veriyi çok çok büyük uzunlukta tutmak ya da veri gelmeye devam ettikçe kendi tampon alanımıza biriktirmek gerekiyordu.

İlk çözüm hiçbir zaman kesin olamayacağından ikinci çözüm denendi. Ancak bu kez _recv()_ fonksiyounun sistemi bloklaması karşı tarafa asla cevap
verilememesine sebep oldu. Bloklanma sorunu _socket.setblocking(0)_ ayarıyla çözülmesine karşın karşı tarafa response iletilmeme sorunu devam etti. Bu kez çözüm ya timeout değeri hesaplamakta ya da headerda tüm uzunluğun tutulmasıyla çözülecekti 2. yöntem seçildi.

### Büyük Veri Gönderme Çözümü

Karşı tarafa mesaj gönderilirken klasik _send()_ metoduyla veri gönderildiğinde karşı tarafın recv_byte uzunluğuna göre yine veri beklediği ve iletişimin sonlanmadığı görüldü. Bu **socket** kütüphanesinin _sendall()_ metoduyla çözüldü.

### Tarayıcıya Response Gönderme Çözümü

İstemci tarafı gerçek bir tarayıcıysa sunucumuzun gönderdiği binary verileri çok kötü gösterebiliyor. Bunun için header kısmına _Content-Disposition: attachment; filename="{}"_ İbaresi eklendi.

### Dosyalama Hiyerarşisi
#### ~/client/

İstemci betiğinin ve gönderilip alınacak dosyaların bulunduğu dizin.

#### ~/server/

Ana sunucu çalıştırma betiğinin, listener sınıflarının bulunduğu dizin.

#### ~/server/files/

Sunucu transfer dosyalarının bulunduğu dizin. Sunucunun ana kodlarının değiştiriliğ silinmemesi için soyutlanmıştır.


#### ~/server/static/

Sunucu tarafı 404, 302, 502 gibi cevap statik html dosyalarının tutulduğu dizin.

#### ~/server/Request/

Request ebeveyn sınıfının ve alt sınıflarının bulunduğu alt Python kütüphanesidir.


#### ~/server/Response/

Response ebeveyn sınıfının ve alt sınıflarının bulunduğu alt python kütüphanesidir.


### Genel Server Sınıf Hiyerarşisi

![Hiyerarşi](./media/hierarchy.png)

**NOT:** Bu bir UML tasarımı değildir.
Kodların okunmasının kolaylaştırması adına Owni_Server yazılımının sınıf hiyearaşisi aşağıdaki gibidir:

### Genel Sunucu Akış Hiyerarşisi


Sunucu yazılımının genel akış hiyararşisi şu biçimdedir.

1. Socket'i verilen ip ve porta bağla
2. Listener nesnesini oluştur ve bağla
3. Mesaj dinle
4. Gelen mesajın ilk boşluk karakterine kadar olan kısmını RequestManager objesine gönder
5. Request nesnesi oluştur.
6. Response nesnesi oluştur.
7. Response.Serialize() ile gelen diziyi cevap olarak gönder.
8. 3. adıma dön.


## Uygulama

### İstemci Tarafları

Sunucumuzun kullanılabileceği birçok uygulama vardır. Test edilenler:

#### Curl

Gnu/Linux ve UNIX sistemlerde dahili gelen komut satırı istemci test programcığı curl ile sunucumuz test edilebilir.

```
curl -X GET 127.0.0.1:6565/files/LoremIpsum.sena
curl -X DELETE 127.0.0.1:6565/files/test.rar
```
#### Tarayıcı

Server uygulamamız Firefox, Chrome, Opera, Edge gibi yazılımlardan da test edilebilir. Bunun için tarayıcının adres çubuğuna isteğinizi yazmanız yeterli.

#### Owni_Client

Sunucu yazılımımız kendi yazdığımız test betiği ile de test edilebilir. Bunun için proje dizininde ~/client/ dizinine gelinip client.py betik programı çalıştırılır.
Karşılayan ekran aşağıdaki gibidir:

![client screenshot](./media/client_1.png)

Buraya server IP adresi, port numarası ve kullanılmak istenen Ip adresi, port numarası ve sunucu dosya yolu girilir. Ardından test menüsü karşınıza çıkar: 

![client screenshot2](./media/client_2.png)

Bu alandan istediğiniz istem ile test yapabilirsiniz.

### Sunucuyu Başlatma

Owni-Server Linux Bash kabuğundan ya da Windows komut satırından (CMD, PowerShell) başlatılır. Bunun için ~/server/ dizinindeyken Python yorumlayıcısının desteği ile sunucu ayağa kaldırılır.

Yazılım komut satırından 2 parametre alır:

1. Ip adresi
2. Port numarası

Örnek bir başlatma örneği aşağıdaki gibidir:
```
python3 main.py 0.0.0.0 6565
```

**NOT:** Port numarasını 80 vermek isterseniz işletim sisteminiz sizden admin ya da SuperUser (su) yetkisi isteyecektir. Bu komuttan sonra sunucunuz size dinlediğini belirten bir mesaj döndürecek ve dinlemeye başlayacaktır. Gelen her isteğin detaylı zamanını, istem tiğini ve isteme ait dosya yolunu ekrana bastıracaktır. Örnek çıktı şu şekildedir:

![server screenshot](./media/server_1.png)

## Sonuç

### Örnek Tarayıcı Çıktıları:

#### 404 Not Found
![not_found](./media/not_found.png)

#### 200 OK
![ok](./media/ok.png)


#### Binary Dosya Aktarımı
![media response](./media/media.png)

### İstekler Boyunca Sunucu Çıktıları
![prints](./media/prints.png)

