header = """HTTP/1.1 {0} {1}\r
Date: {2}\r
Server: Berk_Ay"""

download_header = """Content-Disposition: attachment; filename="{}"
Content-Length: {}"""

class Response:
    def __init__(self, payload):
        self.payload = payload
        
    def serialize(self):
        response = b""
        response += self.generate_header()
        response += self.payload.serialize_byte()

        return response

    def get_type(self):
        raise NotImplementedError("Üzerine Yazılmadı")

    def get_request_code(self):
        raise NotImplementedError("Üzerine Yazılmadı")

    def generate_header(self):
        import datetime
        import Payload

        _header = header.format(self.get_request_code(),
                             self.get_type(),
                             datetime.datetime.now())
        
        if self.payload.get_type() == Payload.Payload.TYPE_BINARY:
            file_name = "sena." + self.payload.ext
            _header += "Content-Type: application/octet-stream; Charset=UTF-8"
            _header += download_header.format(file_name, self.payload.get_size())
            
        else:
            _header += "Content-Type: text/html; Charset=UTF-8"

        _header += "\r\n\r\n"
            
        return _header.encode("UTF-8") 
