import Response.R_Ok as rok
from Response import R_Not_Found
from Response import R_Bad_Request
from Response import R_Created
from Response import R_Teapot

r1 = R_Teapot.Teapot()
print(r1.serialize())

r2 = R_Not_Found.NotFound()
print(r2.serialize())


r3 = R_Bad_Request.BadRequest()
print(r3.serialize())

r4 = R_Created.Created()
print(r4.serialize())
